﻿using System;
using CommandPattern.Autofac.Exceptions;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    public abstract class HandlerBase<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        public readonly ICommandResult CommandResult;

        protected HandlerBase(ICommandResult commandResult)
        {
            CommandResult = commandResult;
        }
        public abstract void Process(TCommand command);

        public ICommandResult Execute(TCommand command)
        {
            try
            {
                Process(command);
            }
            catch (Exception ex)
            {
                var handledErrorException = ex as HandledErrorException;
                if (handledErrorException != null)
                    CommandResult.ErrorList.Add(handledErrorException.CommandMessage);
                
                else
                    throw;
            }
            return CommandResult;
        }
    }
}
