﻿using Autofac;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CommandBus>().As<ICommandBus>().InstancePerDependency();
            builder.RegisterType<ViewHelper>().As<IViewHelper>().InstancePerDependency();
            builder.RegisterType<CommandResult>().As<ICommandResult>().InstancePerLifetimeScope();

        }
    }
}
