﻿using System;
using System.Collections.Generic;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    [Serializable]
    public class CommandResult : ICommandResult
    {
        public CommandResult()
        {
            ErrorList = new List<IMessage>();
            SuccessMessageList = new List<IMessage>();
            InfoMessageList = new List<IMessage>();
            WarningMessageList = new List<IMessage>();
            RedirectOnSuccess = false;
            RedirectDestination = string.Empty;
        }

        public List<IMessage> SuccessMessageList { get; set; }
        public List<IMessage> InfoMessageList { get; set; }
        public List<IMessage> WarningMessageList { get; set; }
        public List<IMessage> ErrorList { get; set; }
        public bool Success { get { return ErrorList.Count == 0; } }
        public bool RedirectOnSuccess { get; set; }
        public string RedirectDestination { get; set; }
    }
}
