﻿using System;
using CommandPattern.Autofac.Exceptions;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    public abstract class ValidationHandlerBase<TCommand> : IValidationHandler<TCommand> where TCommand : ICommand
    {
        public readonly ICommandResult CommandResult;

        protected ValidationHandlerBase(ICommandResult commandResult)
        {
            CommandResult = commandResult;
        }
        public abstract void Process(TCommand command);

        public ICommandResult Validate(TCommand command)
        {
            try
            {
                Process(command);
            }
            catch (Exception ex)
            {
                var handledErrorException = ex as HandledErrorException;
                if (handledErrorException != null)
                    CommandResult.ErrorList.Add(handledErrorException.CommandMessage);
                
                else
                    throw;
            }
            return CommandResult;
        }
    }
}
