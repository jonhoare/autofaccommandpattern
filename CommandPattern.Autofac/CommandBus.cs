using System.Collections.Generic;
using Autofac;
using CommandPattern.Autofac.Exceptions;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    public class CommandBus : ICommandBus
    {
        private readonly List<dynamic> _commandQueue;
        private readonly IComponentContext _context;

        public CommandBus(IComponentContext context)
        {
            _context = context;
            _commandQueue = new List<dynamic>();
        }

        public void AddCommand(ICommand command)
        {
            _commandQueue.Add(command);
        }

        public ICommandResult Validate<TCommand>(TCommand command, bool optional = false) where TCommand : ICommand
        {
            if (_context.IsRegistered<IValidationHandler<TCommand>>())
                return _context.Resolve<IValidationHandler<TCommand>>().Validate(command);
            
            if(!optional)
                throw new NotFoundException(string.Format("Validator not found for {0}", command.GetType().Name));

            return new CommandResult();
        }

        public ICommandResult Execute<TCommand>(TCommand command, bool optional = false) where TCommand : ICommand
        {
            if (_context.IsRegistered<ICommandHandler<TCommand>>())
                return _context.Resolve<ICommandHandler<TCommand>>().Execute(command);
            
            if (!optional)
                throw new NotFoundException(string.Format("Handler not found for {0}", command.GetType().Name));

            return new CommandResult();
        }

        public void Process(bool executeIsOptional = true, bool validateIsOptional = true, bool continueOnError = false)
        {
            foreach (var command in _commandQueue)
            {
                ICommandResult result = Validate(command, validateIsOptional);

                if (!result.Success && !continueOnError)
                    break;

                result = Execute(command, executeIsOptional);

                if (!result.Success && !continueOnError)
                    break;
            }
        }
    }
}