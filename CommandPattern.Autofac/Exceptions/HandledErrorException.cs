﻿using System;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac.Exceptions
{
    public class HandledErrorException : Exception
    {
        public IMessage CommandMessage { get; set; }

        public HandledErrorException()
        {
        }

        public HandledErrorException(IMessage message)
            : base(message.Message)
        {
            CommandMessage = message;
        }

        public HandledErrorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
