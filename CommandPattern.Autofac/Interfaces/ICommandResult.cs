﻿using System.Collections.Generic;

namespace CommandPattern.Autofac.Interfaces
{
    public interface ICommandResult
    {
        List<IMessage> SuccessMessageList { get; set; }
        List<IMessage> InfoMessageList { get; set; }
        List<IMessage> WarningMessageList { get; set; }
        List<IMessage> ErrorList { get; set; }
        bool Success { get; }
        bool RedirectOnSuccess { get; set; }
        string RedirectDestination { get; set; }
    }
}