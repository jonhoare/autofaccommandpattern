﻿namespace CommandPattern.Autofac.Interfaces
{
    public interface IViewHelper
    {
        ICommandBus CommandBus { get; }
        ICommandResult CommandResult { get; }
    }
}
