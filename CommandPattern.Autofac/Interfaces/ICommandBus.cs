﻿using System.Collections.Generic;

namespace CommandPattern.Autofac.Interfaces
{
    public interface ICommandBus
    {
        ICommandResult Execute<TCommand>(TCommand command, bool optional = false) where TCommand : ICommand;
        ICommandResult Validate<TCommand>(TCommand command, bool optional = false) where TCommand : ICommand;
        void AddCommand(ICommand command);
        void Process(bool executeIsOptional = true, bool validateIsOptional = true, bool continueOnError = false);
    }
}
