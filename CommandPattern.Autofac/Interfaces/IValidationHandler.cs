﻿namespace CommandPattern.Autofac.Interfaces
{
    public interface IValidationHandler<in TCommand> where TCommand : ICommand
    {
        ICommandResult Validate(TCommand command);
    }
}
