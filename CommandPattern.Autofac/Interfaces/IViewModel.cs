﻿namespace CommandPattern.Autofac.Interfaces
{
    public interface IViewModel
    {
        ICommandResult CommandResult { get; set; }
    }
}
