﻿namespace CommandPattern.Autofac.Interfaces
{
    public interface IMessage
    {
        string Message { get; }
    }
}
