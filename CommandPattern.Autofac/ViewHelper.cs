﻿using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    public class ViewHelper : IViewHelper
    {
        public ICommandBus CommandBus { get; private set; }
        public ICommandResult CommandResult { get; private set; }

        public ViewHelper(
            ICommandBus commandBus,
            ICommandResult commandResult
        )
        {
            CommandBus = commandBus;
            CommandResult = commandResult;
        }
    }
}
