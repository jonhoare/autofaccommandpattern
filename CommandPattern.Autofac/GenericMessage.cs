﻿using System;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac
{
    [Serializable]
    public class GenericMessage : IMessage
    {
        public string Message { get; private set; }

        public GenericMessage(string message)
        {
            Message = message;
        }
    }
}
