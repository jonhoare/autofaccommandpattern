﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CommandPattern.Autofac.Exceptions;
using CommandPattern.Autofac.Interfaces;
using NUnit.Framework;

namespace CommandPattern.Autofac.Test.Unit
{
    public class AutofacResolverTest
    {
        private IContainer _container;

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new AutofacModule());
            builder.RegisterModule(new CommandPattern.Autofac.AutofacModule());
            _container = builder.Build();
        }

        [Test]
        public void CanResolveICommandBus()
        {
            var commandBus = _container.Resolve<ICommandBus>();

            Assert.IsNotNull(commandBus);
            Assert.IsInstanceOf<CommandBus>(commandBus);
        }
        
        [Test]
        public void CanExecuteSomeTestHandler()
        {
            var command = new SomeTestCommand("Test");

            var commandBus = _container.Resolve<ICommandBus>();
            commandBus.Execute(command);

            var commandResult = _container.Resolve<ICommandResult>();

            Assert.IsTrue(commandResult.Success);
            Assert.IsTrue(commandResult.SuccessMessageList.Any(s => s.Message == "Test"));
        }

        [Test]
        public void CanValidateSomeTest()
        {
            var command = new SomeTestCommand("Test");

            var commandBus = _container.Resolve<ICommandBus>();
            commandBus.Validate(command);

            var commandResult = _container.Resolve<ICommandResult>();

            Assert.IsTrue(commandResult.ErrorList.Any());
        }

        [Test]
        public void CanProcessCommandList()
        {
            var command = new SomeTestCommand("Test");

            var commandBus = _container.Resolve<ICommandBus>();
            commandBus.AddCommand(command);
            commandBus.Process();

            var commandResult = _container.Resolve<ICommandResult>();

            Assert.IsTrue(commandResult.ErrorList.Any());
        }

        [Test]
        public void DoesThrowNotFoundExceptionWhenHandlerNotFound()
        {
            var command = new NoHandlerRegisteredCommand();
            var commandBus = _container.Resolve<ICommandBus>();
            var didFail = false;

            try
            {
                commandBus.Execute(command);
            }
            catch (NotFoundException)
            {
                didFail = true;
            }
            catch (Exception)
            {
                didFail = false;
            }

            Assert.IsTrue(didFail);
        }

        [Test]
        public void DoesThrowNotFoundExceptionWhenValidatorNotFound()
        {
            var command = new NoHandlerRegisteredCommand();
            var commandBus = _container.Resolve<ICommandBus>();
            var didFail = false;

            try
            {
                commandBus.Validate(command);
            }
            catch (NotFoundException)
            {
                didFail = true;
            }
            catch (Exception)
            {
                didFail = false;
            }

            Assert.IsTrue(didFail);
        }
    }
}