﻿using Autofac;
using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac.Test.Unit
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly).AsClosedTypesOf(typeof(ICommandHandler<>)).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(ThisAssembly).AsClosedTypesOf(typeof(IValidationHandler<>)).AsImplementedInterfaces();
        }
    }
}
