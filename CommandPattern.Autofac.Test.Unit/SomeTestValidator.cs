﻿using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac.Test.Unit
{
    public class SomeTestValidator : ValidationHandlerBase<SomeTestCommand>
    {
        public SomeTestValidator(ICommandResult commandResult) : base(commandResult)
        {
            
        }

        public override void Process(SomeTestCommand command)
        {
            if (command.TestValue == "Test")
                CommandResult.ErrorList.Add(new GenericMessage("TestValue was Test!!!"));
        }
    }
}