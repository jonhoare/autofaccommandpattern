using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac.Test.Unit
{
    public class SomeTestCommand : ICommand
    {
        public string TestValue { get; set; }

        public SomeTestCommand(string testValue)
        {
            TestValue = testValue;
        }
    }
}