﻿using CommandPattern.Autofac.Interfaces;

namespace CommandPattern.Autofac.Test.Unit
{
    public class SomeTestCommandHandler : HandlerBase<SomeTestCommand>
    {
        public SomeTestCommandHandler(ICommandResult commandResult) : base(commandResult)
        {
            
        }

        public override void Process(SomeTestCommand command)
        {
            CommandResult.SuccessMessageList.Add(new GenericMessage("Success"));
            CommandResult.SuccessMessageList.Add(new GenericMessage(command.TestValue));
        }
    }
}